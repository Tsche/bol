if GetGame().map.shortName ~= "summonerRift" then return end

function OnLoad()
	PrintChat(">> \"Where are the minions?\" loaded")
	Config = scriptConfig("Where are the minions?", "minions.cfg") 
	Config:addParam("allied", "Draw ally minions?", SCRIPT_PARAM_ONOFF, false)
	Config:addParam("enemy", "Draw enemy minions?", SCRIPT_PARAM_ONOFF, true)
	Config:addParam("vision", "Draw minion vision?", SCRIPT_PARAM_ONOFF, false)
	Config:addParam("spawn", "Draw spawn points?", SCRIPT_PARAM_ONOFF, false)
end

function OnDraw()
	if (GetInGameTimer() > 90) then
		if Config.spawn then
			DrawCircle(917.73071289063, 124.57879638672, 1720.3618164063, 100, 0x00FF6600)
			DrawCircle(1446.0969238281, 118.8975982666, 1664.8077392578, 100, 0x00FF0000)
			DrawCircle(1546.0969238281, 111.27220916748, 1314.8077392578, 100, 0x000022FF)
		
			DrawCircle(13062.497070313, 119.21710205078, 12760.784179688, 100, 0x00006600)
			DrawCircle(12511.7734375, 110.7853012085, 12776.9296875, 100, 0x00FF22FF)
			DrawCircle(12451.052734375, 128.45840454102, 13217.54296875, 100, 0x00FF0000)
		end
		
		timer = (GetInGameTimer()%60 > 30 and GetInGameTimer() - 30 or GetInGameTimer())
		first = 325*(timer%60)
		last = 325*((timer-6)%60)
	
		if (myHero.team == TEAM_BLUE and Config.allied) or (myHero.team == TEAM_RED and Config.enemy) then
			if Config.vision then
				DrawCircle(917, 124, 1720 + first, 1200, ARGB(255, 0, 102, 255))
				DrawCircle(917, 124, 1720 + ((first+last)/2), 1200, ARGB(255, 0, 102, 255))
				DrawCircle(917, 124, 1720 + last, 1200, ARGB(255, 0, 102, 255))
	
				DrawCircle(1446 + (22/30)*first, 118, 1664 + (22/30)*first, 1200, ARGB(255, 0, 102, 255))
				DrawCircle(1446 + (22/30)*((first+last)/2), 118, 1664 + (22/30)*((first+last)/2), 1200, ARGB(255, 0, 102, 255))
				DrawCircle(1446 + (22/30)*last, 118, 1664 + (22/30)*last, 1200, ARGB(255, 0, 102, 255))
	
				DrawCircle(1546 + first, 124, 1314, 1200, ARGB(255, 0, 102, 255))
				DrawCircle(1546 + ((first+last)/2), 124, 1314, 1200, ARGB(255, 0, 102, 255))
				DrawCircle(1546 + last, 124, 1314, 1200, ARGB(255, 0, 102, 255))
			end
	
			DrawCircleMinimap(917, 124, 1720 + first)
			if 1720 + last < 14527 then
				DrawCircleMinimap(917, 124, 1720 + last)
				DrawLine(GetMinimapX(917), GetMinimapY(1720 + first), GetMinimapX(917), GetMinimapY(1720 + last), 5, ARGB(255, 0, 102, 255))
			end
		
			DrawCircleMinimap(1446 + (22/30)*first, 118, 1664 + (22/30)*first)
			DrawCircleMinimap(1446 + (22/30)*last, 118, 1664 + (22/30)*last)
			if 1446 + (22/30)*last < (14279/2) and 1664 + (22/30)*last < (14527/2) then
				DrawLine(GetMinimapX(1446 + (22/30)*first), GetMinimapY(1664 + (22/30)*first), GetMinimapX(1446 + (22/30)*last), GetMinimapY(1664 + (22/30)*last), 5, ARGB(255, 0, 102, 255))
			end
	
			DrawCircleMinimap(1546 + first, 124, 1314)
			if 1546 + last < 14527 then
				DrawCircleMinimap(1546 + last, 124, 1314)
				DrawLine(GetMinimapX(1546 + first), GetMinimapY(1314), GetMinimapX(1546 + last), GetMinimapY(1314), 5, ARGB(255, 0, 102, 255))
			end
		end
	
		if (myHero.team == TEAM_RED and Config.allied) or (myHero.team == TEAM_BLUE and Config.enemy) then
			if Config.vision then
				DrawCircle(12451 + (-1) * first, 124, 13217, 1200, ARGB(255, 255, 0, 0))
				DrawCircle(12451 + (-1) * ((first+last)/2), 124, 13217, 1200, ARGB(255, 255, 0, 0))
				DrawCircle(12451 + (-1) * last, 124, 13217, 1200, ARGB(255, 255, 0, 0))
	
				DrawCircle(12511 + (-22/30) * first, 118, 12776 + (-22/30) * first, 1200, ARGB(255, 255, 0, 0))
				DrawCircle(12511 + (-22/30) * ((first+last)/2), 118, 12776 + (-22/30) * ((first+last)/2), 1200, ARGB(255, 255, 0, 0))
				DrawCircle(12511 + (-22/30) * last, 118, 12776 + (-22/30) * first, 1200, ARGB(255, 255, 0, 0))
	
				DrawCircle(13062, 128, 12760 + (-1) * first, 1200, ARGB(255, 255, 0, 0))
				DrawCircle(13062, 128, 12760 + (-1) * ((first+last)/2), 1200, ARGB(255, 255, 0, 0))
				DrawCircle(13062, 128, 12760 + (-1) * last, 1200, ARGB(255, 255, 0, 0))
			end
	
			DrawCircleMinimap(12451 + (-1) * first, 124, 13217)
			if 12451 + (-1) * last > 0 then
				DrawCircleMinimap(12451 + (-1) * last, 124, 13217)
				DrawLine(GetMinimapX(12451 + (-1) * first), GetMinimapY(13217), GetMinimapX(12451 + (-1) * last), GetMinimapY(13217), 5, ARGB(255, 255, 0, 0))
			end
	
			DrawCircleMinimap(12511 + (-22/30) * first, 118, 12776 + (-22/30) * first)
			if 12511 + (-22/30)*last > (14279/2) and 12776 + (-22/30)*last > (14527/2) then
				DrawCircleMinimap(12511 + (-22/30) * last, 118, 12776 + (-22/30) * last)
				DrawLine(GetMinimapX(12511 + (-22/30) * first), GetMinimapY(12776 + (-22/30) * first), GetMinimapX(12511 + (-22/30) * last), GetMinimapY(12776 + (-22/30) * last), 5, ARGB(255, 255, 0, 0))
			end
	
			DrawCircleMinimap(13062, 128, 12760 + (-1) * first)
			if 12760 + (-1) * last > 0 then
				DrawCircleMinimap(13062, 128, 12760 + (-1) * last)
				DrawLine(GetMinimapX(13062), GetMinimapY(12760 + (-1) * first), GetMinimapX(13062), GetMinimapY(12760 + (-1) * last), 5, ARGB(255, 255, 0, 0))
			end
		end
	end
end